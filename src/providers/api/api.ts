import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiProvider {

	//private API_URL = 'http://localhost/agenda/api/v1/';
	private API_URL = 'http://quartetooffice.p3sistemas.com/v1/';

	constructor(public http: Http){}

	loginApi(login:any, senha:any){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL+`login?user=${login}&pass=${senha}`;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	getGrupos(){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL+`grupos`;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	getAgendaByGrupoIdAndData(grupoId:any, mes:any, ano:any){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL+`agendas?grupoId=${grupoId}&mes=${mes}&ano=${ano}`;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	getAgendaByDataAndGrupoId(data:string, grupoId:any){
		return new Promise((resolve, reject) => {
		  let url = this.API_URL+`agenda_by_data?data=${data}&grupoId=${grupoId}`;
		  this.http.get(url)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}

	setAgenda(dados:any[]){
		return new Promise((resolve, reject) => {
		  var data = {dados:dados};
		  this.http.post(this.API_URL+'cadastrar_agenda', dados)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}
	
	// o método PUT deu erro, então vamos de POST
	updateAgenda(dados:any[]){
		return new Promise((resolve, reject) => {
		  var data = {dados:dados};
		  this.http.post(this.API_URL+'atualizar_agenda', dados)
		    .subscribe((result: any) => {
		      resolve(result.json());
		    },
		    (error) => {
		      reject(error.json());
		    });
		});
	}
}
