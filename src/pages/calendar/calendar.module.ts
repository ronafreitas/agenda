import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalendarPage } from './calendar';
import {MycalComponentModule} from '../../components/mycal/mycal.module'
@NgModule({
  declarations: [
    CalendarPage
  ],
  imports: [
  	MycalComponentModule,
    IonicPageModule.forChild(CalendarPage),
  ],
})
export class CalendarPageModule {}
