import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController,Platform} from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {

	currentDate:string
	changeCalendario:any=[]
	datas:any=[]
	comDts:any=[]
	anos:any = ['2019','2020','2021','2022','2023']
	meses:any = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
	dias:any = ['SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB', 'DOM']
	dataa:any
	diaparatim:any
	mesCorrente:any
	anoCorrente:any

	constructor(public navCtrl: NavController, private loadingCtrl: LoadingController, private api:ApiProvider,private push: Push,public plt: Platform){}

	ionViewDidLoad(){

		const d = new Date()
		const m = d.getMonth()
		const a = d.getFullYear()
		//this.showCalendar(m , a)
		this.mesCorrente = m
		this.anoCorrente = a

		this.inicioCalendario(m, a)

	    if(this.plt.is('cordova')){
	      // This will only print when on iOS
	      console.log('I am an iOS device!');
	      this.pushSetup()
	    }else{

	    }

		this.intToMes(this.mesCorrente, this.anoCorrente)
	}


	inicioCalendario(mes, ano){
		let self = this
		let agendas = []
		let usuario = JSON.parse( localStorage.getItem('usuario') )
		this.api.getAgendaByGrupoIdAndData(usuario.grupoId, mes, ano)
		.then((res: any) => {
			this.presentLoadingWithOptions()
			res.data.forEach(function(d){
				let dt  = new Date(d.Start)
				let ndd = self.converteDataGetDia(dt)
				let mnt = self.converteDataGetMes(dt)
				let and = self.converteDataGetAno(dt)
				if(agendas[ndd]){
					let corr = parseInt(d.LabelColor)
					agendas[ndd].agenda.push({texto:d.Caption,cor:self.decimalToHexa(corr)})
				}else{
					let corr = parseInt(d.LabelColor)
					agendas[ndd] = {year: and,month: mnt,date: ndd,agenda:[{texto:d.Caption,cor:self.decimalToHexa(corr)}]}
				}
			})

			agendas.forEach(function(a){
				self.changeCalendario.push(a)
			})
		})
		.catch((error: any) => {
			console.log('error1',error);
		})
	}

	decimalToHexa(hexa:number):string{
		let nhexa = hexa.toString(16)
		return '#'+nhexa
	}

	intToMes(mes, ano){
		this.currentDate = this.meses[mes]+' '+ano
	}

	onDaySelect(e){
		let dt = Math.round(new Date(e.year,e.month,e.date).getTime()/1000)
		this.navCtrl.push('TimelinePage', {dt});
	}

	onMonthSelect(e){
		this.inicioCalendario(e.month, e.year)
		this.intToMes(e.month, e.year)
	}

	converteDataGetDia(data:any){
		var dia=data.getDate();
		if(dia < 10)
			dia = '0'+dia.toString()
		return dia
	}

	converteDataGetMes(data:any){
		return data.getMonth();
	}

	converteDataGetAno(data:any){
		return data.getFullYear();
	}

	async presentLoadingWithOptions(){
		const loading = await this.loadingCtrl.create({
			spinner: "bubbles",
			duration: 800,
			content: "Aguarde...",
		});
		return await loading.present();
	}

	selecionaCalendario(month, year, alterado=null){

		if(alterado)
			month = this.mesToInt(month)

	    var loading = this.loadingCtrl.create({
	      spinner: "bubbles",
	      content: "Aguarde..."
	    })
	    loading.present()

		//let self = this
		//let dtcompe = new Date(year, month)
	    //let firstDay = (dtcompe).getDay();
	    //let daysInMonth = 32 - new Date(year, month, 32).getDate();

	    let tbl = document.getElementById("calendar-body");

	    tbl.innerHTML = "";

	    this.dataa = 1;

		this.currentDate = this.meses[month] +' '+year
		let usuario = JSON.parse( localStorage.getItem('usuario') );

		this.api.getAgendaByGrupoIdAndData(usuario.grupoId, month, year)
		.then((res: any) => {

		})
		.catch((error: any) => {
			console.log('error1',error);
		})
		loading.dismiss()
	}
	
	mesToInt(mes:any){
		var ind;
		for(var m in this.meses){
			if(mes == this.meses[m])
				ind = m
		}
		return ind
	}

	chunkArray(myArray, chunk_size){ 
		let index = 0;
		let arrayLength = myArray.length;
		let tempArray = [];
		let myChunk;
		for(index = 0; index < arrayLength; index += chunk_size)
		    myChunk = myArray.slice(index, index+chunk_size);
		    tempArray.push(myChunk);

		return tempArray;
	}

	datasDoMes(mes, ano){
		const date = new Date(ano, mes)
		const days = []
		let cData;
		while(date.getMonth() === mes){
			cData = new Date(date)
			const ndat = cData.getDate()
			const num = cData.getDay()
			days.push({data:Math.floor(cData /1000),dia:ndat,ordem:num})
			date.setDate(date.getDate() + 1)
		}
		return days
	}

	create(){
		this.navCtrl.push('CreatePage');
	}

	timeline(dt){
		//dt = Math.round(new Date(year,month,self.diaparatim).getTime()/1000)
		this.navCtrl.push('TimelinePage', {dt});
	}


	pushSetup(){

		this.push.hasPermission()
		  .then((res: any) => {
		    if(res.isEnabled){
		      console.log('We have permission to send push notifications');
		    }else{
		      console.log('We do not have permission to send push notifications');
		    }
		});

		let usuario = JSON.parse( localStorage.getItem('usuario') );
		let topic = 'grupo'+usuario.grupoId
		
		// to initialize push notifications
		const options: PushOptions = {
		   android: {
		   		senderID: '489562934108',
                sound: true,
                vibrate: true,
                topics: [topic]
		   },
		   ios: {
		       alert: 'true',
		       badge: true,
		       sound: 'false'
		   },
		   windows: {},
		   browser: {}
		}

		const pushObject: PushObject = this.push.init(options);
		pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
		pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
		pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

		//registration.registrationId

	}


}
