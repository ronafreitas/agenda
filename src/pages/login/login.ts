import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	usuario:string;
	senha:string;
	falhaLogin:boolean=false;

	constructor(public navCtrl: NavController, private loadingCtrl: LoadingController,  private api:ApiProvider){}

	ionViewDidLoad(){
		localStorage.removeItem('usuario');
	}

	login(){
	    var loading = this.loadingCtrl.create({
	      spinner: "bubbles",
	      content: "Acessando..."
	    });

	    loading.present();

		this.api.loginApi(this.usuario, this.senha)
		.then((res: any) => {
			localStorage.setItem('usuario', JSON.stringify(res.data[0]));
			loading.dismiss();
			if(res.data.length > 0){
				this.falhaLogin=false;
				//this.navCtrl.push('HomePage');
				this.navCtrl.setRoot('CalendarPage');
			}else{
				this.falhaLogin=true;
			}			
		}).catch((error: any) => {
			console.log('error1',error);
		});
	}

	goToSignupPage() {
		this.navCtrl.push('SignupPage');
	}

	goToHomePage() {
		this.navCtrl.push('HomePage');
	}

}
