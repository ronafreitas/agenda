import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

	character:any
	meses:any = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
	anos:any = ['2019','2020','2021','2022','2023']

	mesSelecionado:any
	anoSelecionado:any
	
 	constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController){

 		this.anoSelecionado = this.navParams.data.anoCorrente
 		let mescurt = this.navParams.data.mesCorrente

 		if(typeof mescurt == 'string'){
 			this.mesSelecionado = mescurt
 		}else{
 			this.mesSelecionado = this.meses[mescurt]
 		}
 	}

	dismiss(){
		this.viewCtrl.dismiss();
	}

	selecionarData(){
		this.viewCtrl.dismiss({ano: this.anoSelecionado,mes:this.mesSelecionado});
	}

	selAno(an:any){
		this.anoSelecionado = an
	}

	selMes(me:any){
		this.mesSelecionado = me
	}

}
