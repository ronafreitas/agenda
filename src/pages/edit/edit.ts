import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController,AlertController,NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {

	titulo:string
	descri:string
	local:string
	fromDate: any
	toDate: any
	grupos:any
	seleGrupoId:any
	dadosGravar:any
	grupoId:any
	IDAGENDA:number

	colors: Array<string> = ["#84edf5","#ffb675", "#e6e6d5", "#ffe675", "#ff9585", "#a5de64", "#859fe6", "#c4a6f5", "#d4cc83", "#a5cfc6"];
	color: string = "#84edf5";
	labelColoreSelecionada:string = "#84edf5";

	constructor(public navCtrl: NavController, public navParams: NavParams, private api:ApiProvider,private loadingCtrl: LoadingController,public alertController: AlertController){}

	ionViewDidLoad(){

		const dataEdit = this.navParams.data.agd
		this.IDAGENDA = dataEdit.ID
		this.titulo = dataEdit.Caption
		this.descri = dataEdit.Message
		this.local = dataEdit.Location
		this.fromDate = new Date(dataEdit.Finish).toISOString()
		this.toDate = new Date(dataEdit.Start).toISOString()
		this.color = dataEdit.LabelColor
		this.grupoId = dataEdit.grupoId
		this.seleGrupoId = dataEdit.grupoId

		this.api.getGrupos()
		.then((res: any) => {
			this.grupos = res.data
		})
		.catch((error: any) => {
			console.log('error1',error);
		});
	}

	prepareColorSelector() {
		setTimeout(() => {
			let buttonElements = document.querySelectorAll('div.alert-radio-group button');
			if (!buttonElements.length) {
				this.prepareColorSelector();
			} else {
				for (let index = 0; index < buttonElements.length; index++) {
					let buttonElement = buttonElements[index];
					let optionLabelElement = buttonElement.querySelector('.alert-radio-label');
					let color = optionLabelElement.innerHTML.trim();

					if (this.isHexColor(color)) {
						buttonElement.classList.add('colorselect', 'color_' + color.slice(1, 7));
						if (color == this.color) {
							buttonElement.classList.add('colorselected');
						}
					}
				}
			}
		}, 100);
	}

	isHexColor(color) {
		let hexColorRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/;
		return hexColorRegEx.test(color);
	}

	selectColor(color) {
		let buttonElements = document.querySelectorAll('div.alert-radio-group button.colorselect');
		for (let index = 0; index < buttonElements.length; index++) {
			let buttonElement = buttonElements[index];
			buttonElement.classList.remove('colorselected');
			if (buttonElement.classList.contains('color_' + color.slice(1, 7))) {
				buttonElement.classList.add('colorselected');
			}
		}
	}

	setColor(color){
		this.labelColoreSelecionada = color
	}

	voltar(){
		this.navCtrl.pop();
	}

	converteData(data:any){
		var dia=data.getDate();
		if(dia < 10){
			dia = '0'+dia.toString()
		}
		var mes=data.getMonth();
		mes++
		if(mes < 10){
			mes = '0'+mes.toString()
		}
		var ano=data.getFullYear();
		
		return ano+'-'+dia+'-'+mes
	}

	salvar(){

		let dateFrom = new Date(this.fromDate)
		let fromDate2 = this.converteData(dateFrom)
		let dateTo = new Date(this.toDate)
		let toDate2 = this.converteData(dateTo)

		if(this.titulo == undefined || this.descri == undefined || this.local == undefined || this.seleGrupoId == undefined){
			this.presentAlert('Ops!','Necessário preencher todos os campos')
			console.log('erro')
			return false
		}else{
			console.log('ok')
		}

	    var loading = this.loadingCtrl.create({
	      spinner: "bubbles",
	      content: "Aguarde..."
	    });
	    loading.present();

		this.dadosGravar = {
			ID:this.IDAGENDA,
			Caption:this.titulo,
			Message:this.descri,
			Location:this.local,
			Start:fromDate2,
			Finish:toDate2,
			grupoId:this.seleGrupoId,
			LabelColor:this.hexaToDecimal(this.labelColoreSelecionada)
		}
		
		this.api.updateAgenda(this.dadosGravar)
		.then((res: any) => {
			loading.dismiss()
			this.navCtrl.setRoot('CalendarPage')
		})
		.catch((error: any) => {
			loading.dismiss()
			console.log(error)
			this.presentAlert('Não foi possível atualizar', 'Tente novamente')
		})
	}

	hexaToDecimal(hexa:string):number{
		return parseInt(hexa.substring(1), 16)
	}

	decimalToHexa(hexa:number):string{
		let nhexa = hexa.toString(16)
		return '#'+nhexa
	}

	getGrupoOpt(ev:any):void{
		this.seleGrupoId = ev
	}

	presentAlert(msg:string, sbti:string){
	  let alert = this.alertController.create({
	    title: msg,
	    subTitle: sbti,
	    buttons: ['Entendi']
	  });
	  alert.present();
	}
}
