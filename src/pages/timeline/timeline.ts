import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
@IonicPage()
@Component({
  selector: 'page-timeline',
  templateUrl: 'timeline.html',
})
export class TimelinePage{

	dataAgenda:any
	dataToGo:any
	agendas:any=[]

	constructor(public navCtrl: NavController, private loadingCtrl: LoadingController, public navParams: NavParams, private api:ApiProvider){}

	ionViewDidLoad(){
	    var loading = this.loadingCtrl.create({
	      spinner: "bubbles",
	      content: "Aguarde..."
	    })
	    loading.present()

	    //console.log(this.navParams.data)
		const dtsc = this.navParams.data.dt

		//const date3 = new Date(dtsc * 1000).toLocaleDateString("pt-BR")
		let usuario = JSON.parse( localStorage.getItem('usuario') );
		this.dataToGo = dtsc
		this.dataAgenda = this.timeConverter( dtsc )
		let self = this
		this.api.getAgendaByDataAndGrupoId(dtsc,usuario.grupoId)
		.then((res: any) => {

			loading.dismiss()

			if(res.data.length > 0){
				res.data.forEach(function(i){
					let cor = parseInt(i.LabelColor)
					self.agendas.push({
						ID:i.ID,
						Caption:i.Caption,
						Message:i.Message,
						Location:i.Location,
						Start:i.Start,
						Finish:i.Finish,
						grupoId:i.grupoId,
						LabelColor:self.decimalToHexa(cor),
					})
				})
			}
		}).catch((error: any) => {
			loading.dismiss()
			console.log('error1',error)
		});

	}

	decimalToHexa(hexa:number):string{
		let nhexa = hexa.toString(16)
		return '#'+nhexa
	}

	voltar(){
		this.navCtrl.pop()
	}

	create(dt:any){
		this.navCtrl.push('CreatePage',{dt})
	}

	timeConverter(UNIX_timestamp){
		var a = new Date(UNIX_timestamp * 1000)
		var months = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
		var year = a.getFullYear()
		var month = months[a.getMonth()]
		var date = a.getDate()
		return date + ' ' + month + ' ' + year
	}

	editar(agd:any){
		this.navCtrl.push('EditPage',{agd})
	}

	/*timeConverter(UNIX_timestamp){
		var a = new Date(UNIX_timestamp * 1000)
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
		var year = a.getFullYear()
		var month = months[a.getMonth()]
		var date = a.getDate()
		var hour = a.getHours()
		var min = a.getMinutes()
		var sec = a.getSeconds()
		var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
		return time;
	}*/
}
